
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    'iview/dist/styles/iview.css',
    'quill/dist/quill.snow.css',
    'quill/dist/quill.bubble.css',
    'quill/dist/quill.core.css',
    '@/assets/css/default.scss',
    // default-theme-theme:
    '@/assets/css/default-theme/component.scss',
    '@/assets/css/default-theme/page.scss',
    // light-theme:
    '@/assets/css/light-theme/component.scss',
    '@/assets/css/light-theme/global.scss',
    '@/assets/css/light-theme/page.scss',
    '@/assets/css/light-theme/plugin.scss',
    // night-theme:
    '@/assets/css/night-theme/component.scss',
    '@/assets/css/night-theme/global.scss',
    '@/assets/css/night-theme/page.scss',
    '@/assets/css/night-theme/plugin.scss',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/iview',
    '@/plugins/api',
    '@/plugins/apiErrorHandler',
    '@/plugins/conditionMutator',
    { src: '@/plugins/redirectTo.js', ssr: false },
    { src: '@/plugins/displayBodyBlock.js', ssr: false },
    { src: '@/plugins/scrollToTop.js', ssr: false },
    { src: '@/plugins/editor.js', ssr: false }
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/eslint-module'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },
  router: {
    middleware: 'auth',
  },
}
