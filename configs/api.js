const apiRoot = 'https://68f26fc6.ngrok.io/api';
const mode = 'dev'; // dev, production
const status = {
  apiError: 550,
  requestValidationError: 422,
};

export {
  apiRoot,
  mode,
  status,
};
