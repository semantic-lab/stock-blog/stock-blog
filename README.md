# stock-blog
基於 SSR Nuxt.js 的個人股市部落格。

此部落格使用的框架如下:
01. MVVM 架構
02. SSR Nuxt.js (server side render 版本的 Vue.js)
03. docker 模式部屬

## user
系統分為"**一般使用者**"與"**系統管理者**"兩種身分，
並依不同身分讀取不同資料與使用不同功能，更多介紹如下。

* 一般使用者
    * 股市資料瀏覽
    * 不同股市比較
    * 個股提醒點 (價格, 日期, 量, ...)
    * 個人通知
    * 個人筆記

* 管理者
    * 資料管理 (tag)
    * 使用者管理
    * 筆記管理

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
