function todayLocaleString() {
  return (new Date()).toLocaleDateString()
}

function todayLocaleTimeStamp(timePart = '00:00:00') {
  const datePart = todayLocaleString();
  const todayAtZero = new Date(`${datePart} ${timePart}`);
  return todayAtZero.getTime();
}

export {
  todayLocaleString,
  todayLocaleTimeStamp,
}
