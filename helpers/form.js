import { LaraValidator } from 'lara-validator';

const mixin = {
  data() {
    return {
      formStyle: {
        hidden: {
          opacity: '0',
          width: '0',
          height: '0',
          marginBottom: '0',
        },
        show: {
          opacity: '1',
          width: '100%',
          height: 'auto',
          marginBottom: '40px',
        }
      },
    };
  },
  methods: {
    schemaFields(schema) {
      return Object.keys(schema);
    },
    validate(form) {
      const data = form.data;
      const validator = form.validator;
      validator.setData(data);
      const isValid = validator.valid();
      form.error = validator.errorMessage;
      return isValid;
    },
    initFormValidators(forms) {
      forms.forEach((form) => {
        const rules = form.rules;
        form.validator = new LaraValidator();
        form.validator.setRules(rules);
      });
    },
  },
};

const basicForm = {
  schema: {},
  rules: {},
  data: {},
  error: {}
};

export {
  mixin,
  basicForm,
};
