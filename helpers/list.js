const columns = {
  selection: {
    type: 'selection',
    width: 60,
    align: 'center',
    fixed: 'left',
  },
  index: {
    type: 'index',
    width: 100,
    align: 'center',
    fixed: 'left',
  },
  id: {
    title: 'ID',
    key: 'id',
    width: 120,
    align: 'center',
    fixed: 'left',
  },
};

export {
  columns,
};
