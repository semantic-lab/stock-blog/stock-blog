export default class APIError extends Error {
  constructor({ status, message }, data = undefined) {
    super(message);
    this.status = status;
    this.message = message;
    this.data = data;
  }

  toObject() {
    return {
      status: this.status,
      message: this.message,
      data: this.data,
    }
  }
}

APIError.CLIENT_REQUEST_VALIDATION = { status: 0, message: 'API Request Validation Error At Client' };
APIError.SERVER_REQUEST_VALIDATION = { status: 1, message: 'API Request Validation Error At Server' };
APIError.SERVER = { status: 2, message: 'API Server Error' };
APIError.INVOKE = { status: 3, message: 'API Invoke Error' };
