import { mapGetters } from 'vuex';

const eventsNames = [
  // Acceleration
  'setMoveThreshold',
  'setShakeThreshold',
  'deviceMoved',
  'deviceTurned',
  'deviceShaken',
  // Keyboard
  'keyPressed',
  'keyReleased',
  'keyTyped()',
  'keyIsDown()',
  // Mouse
  'mouseMoved',
  'mouseDragged',
  'mousePressed',
  'mouseReleased',
  'mouseClicked',
  'doubleClicked',
  'mouseWheel',
  // Touch
  'touchStarted',
  'touchMoved',
  'touchEnded',
  // Environment
  'print',
  'cursor',
  'frameRate',
  'noCursor',
  'windowResized',
  'fullscreen',
  'pixelDensity',
  'displayDensity',
  'getURL',
  'getURLPath',
  'getURLParams',
  // Structure
  'preload',
  'setup',
  'draw',
];

const domEventNames = [
  'click',
  'contextmenu',
  'dblclick',
  'mousedown',
  'mouseenter',
  'mouseleave',
  'mousemove',
  'mouseover',
  'mouseout',
  'mouseup',
  'keydown',
  'keypress',
  'keyup',
  'blur',
  'change',
  'focus',
  'focusin',
  'focusout',
  'input',
  'invalid',
  'reset',
  'search',
  'select',
  'submit',
  'drag',
  'dragend',
  'dragenter',
  'dragleave',
  'dragover',
  'dragstart',
  'drop',
  'copy',
  'cut',
  'paste',
  'mousewheel',
  'wheel',
  'touchcancel',
  'touchend',
  'touchmove',
  'touchstart'
];

const script = (vue) => {
  const result = {};
  eventsNames.forEach((eventName) => {
    result[eventName] = (sketch, $event) => {
      const methodName = `_${eventName}`;
      if (vue[methodName]) {
        vue[methodName](sketch, $event);
      }
    };
  });
  return result;
};

const actions = {
  sizeAction({width, height, leftTopX, rightTopX, leftTopY, leftBottomY}) {
    const canvas = this.canvas;
    canvas.width = width;
    canvas.height = height;
    canvas.dom = {leftTopX, rightTopX, leftTopY, leftBottomY};
    this.calculateCoordinateSystemBound();
    this.transDataToPoints();
  },
};

const emits = {
  emitPoint(key, index) {
    this.$emit('on-point', {key, index});
  },
  emitLoadMoreData(difference, type) {
    this.$emit('on-load-more-data', {difference, type});
  }
};

const events = {
  _mouseMoved(sketch) {
    if (this.inDom(sketch) && !this.lockP5Drawing) {
      const canvas = this.canvas;
      const data = this.data;
      const point = this.getCurrentPoint(sketch);
      canvas.currentPoint = (point) ? data[point.key] : {};

      // emit the current point to the parent component for sync
      // only if free move is true
      if (this.syncCrossHair && this.freeMove) {
        const key = (point) ? point.key : undefined;
        const index = (point) ? canvas.show.points.indexOf(point) : -1;
        this.emitPoint(key, index);
      }
    }
  },
  _mousePressed(sketch) {
    if (this.inDom(sketch) && !this.lockP5Drawing) {
      this.canvas.dragged.last = { x: sketch.mouseX, y: sketch.mouseY};
    }
  },
  _mouseDragged(sketch) {},
  _mouseReleased(sketch) {
    if (this.inDom(sketch) && !this.lockP5Drawing) {
      this.canvas.dragged = { last: {}, current: {} };
    }
  },
  _mouseWheel(sketch, $event) {
    if (this.inDom(sketch) && !this.lockP5Drawing) {
      const difference = $event.delta / Math.abs($event.delta);
      this.loadMoreData(sketch, difference, 'wheel');
    }
  },
  _keyReleased(sketch) {
    const pressESCKey = sketch.keyCode === 27;
    if (pressESCKey) {
      this.lockP5Drawing = !this.lockP5Drawing;
    }
  }
};

const functions = {
  inDom(sketch) {
    if (sketch) {
      const dom = this.canvas.dom;
      const currentX = sketch.mouseX + dom.leftTopX;
      const currentY = sketch.mouseY + dom.leftTopY;
      const inDomX = (dom.leftTopX <= currentX && currentX <= dom.rightTopX );
      const inDomY = (dom.leftTopY <= currentY && currentY <= dom.leftBottomY );
      return inDomX && inDomY;
    }
  },
  inXBound(pointX) {
    const xBound = this.canvas.width || 0;
    return pointX <= xBound;
  },
  inYBound(pointY) {
    const yBound = this.canvas.height || 0;
    return pointY <= yBound;
  },
  inCoordinateSystem(point) {
    const coordinate = this.canvas.coordinate;
    const inXBound = coordinate.leftBottom.x <= point.x && point.x <= coordinate.rightBottom.x;
    const inYBound = coordinate.leftTop.y <= point.y && point.y <= coordinate.leftBottom.y;
    return inXBound && inYBound;
  },
  getDataIndexBoundOfShowingPoint() {
    const lowerBound = this.canvas.show.atDataIndex;
    let upperBound = this.canvas.show.atDataIndex + this.dataAmount - 1;
    const isOverBounds = upperBound >= this.dataKeys.length + 1;
    // set the last index as index of last data
    if (isOverBounds) {
      upperBound = this.dataKeys.length;
    }
    return { lowerBound, upperBound };
  },
  getDataFieldValueBound(dataIndexLowerBound, dataIndexUpperBound, dataFieldKey) {
    const fieldValues = [];
    for (let i = dataIndexLowerBound; i < dataIndexUpperBound; i++) {
      const key = this.dataKeys[i];
      const ele = this.data[key];
      const fieldValue = ele[dataFieldKey];
      if (fieldValue) {
        fieldValues.push(fieldValue);
      }
    }
    return {
      max: Math.max(...fieldValues),
      min: Math.min(...fieldValues),
    };
  },
  setDataValueBound(dataFields = []) {
    const { lowerBound, upperBound } = this.getDataIndexBoundOfShowingPoint();
    const maxData = [];
    const minData = [];
    dataFields.forEach(({dataField}) => {
      const {max, min} = this.getDataFieldValueBound(lowerBound, upperBound, dataField);
      maxData.push(max);
      minData.push(min);
    });
    this.canvas.show.maxDataValue = Math.max(...maxData);
    this.canvas.show.minDataValue = Math.min(...minData);
  },
  dataToPoint(dataIndex, value) {
    const leftBottom = this.canvas.coordinate.leftBottom;
    if (leftBottom) {
      const dataIndexOfFirstShowingPoint = this.canvas.show.atDataIndex;
      const xUnitAmount = dataIndex - dataIndexOfFirstShowingPoint;
      if (xUnitAmount >= 0) {
        const yUnitAmount = (value - this.canvas.show.minDataValue) / this.canvas.unitPrice;
        return {
          x: leftBottom.x + this.canvas.xUnit * (xUnitAmount + 1),
          y: leftBottom.y - this.canvas.yUnit * yUnitAmount - this.canvas.yAddHeight,
        };
      }
      console.warn('dataToPoint: given data index is less than data index of first showing point');
    }
    console.warn('dataToPoint: Coordinate System is not ready');
    return undefined;
  },
  transDataToPoints() {
    // reset points
    this.canvas.show.points = [];
    this.canvas.show.maxDataValue = undefined;
    this.canvas.show.minDataValue = undefined;
    this.setDataValueBound(this.dataFields);

    // update unit
    this.calculateUnit();

    // translate
    const {lowerBound, upperBound} = this.getDataIndexBoundOfShowingPoint();
    for (let i = lowerBound; i < upperBound; i++) {
      const key = this.dataKeys[i];
      const dataOfPoint = this.data[key];
      let x = undefined;
      const y = {};
      this.dataFields.forEach(({dataField}) => {
        const point = this.dataToPoint(i, dataOfPoint[dataField]);
        // set x value of point
        if (!x && point.x) {
          x = point.x;
        }
        // set several y values of point
        y[dataField] = point.y;
      });
      // add point into showing point list
      this.canvas.show.points.push({ key, x, y });
    }
  },
  calculateTransferMap() {
    /*
    * Difference | unit price | unit height | total units |
    *  10        | 0.5        | h/(20 + 5) | 20 + 5
    *  20        | 0.5        | h/(40 + 5) | 40 + 5
    *  50        | 1.0        | h/(50 + 10) | 50 + 10
    * 100        | 2.0        | h/(50 + 10) | 50 + 10
    * */
    const result = {};
    const unitMappers = [
      { difference: 1, unitPrice: 0.2, totalUnits: 5 },
      { difference: 3, unitPrice: 0.5, totalUnits: 7 },
      { difference: 6, unitPrice: 0.5, totalUnits: 12 },
      { difference: 10, unitPrice: 0.5, totalUnits: 20 + 5 },
      { difference: 50, unitPrice: 1, totalUnits: 50 + 10 },
      { difference: 80, unitPrice: 2, totalUnits: 40 + 10 },
    ];
    const dataValueDifference = this.canvas.show.maxDataValue - this.canvas.show.minDataValue;
    let index = 0;
    while (true) {
      let breakOut = false;
      unitMappers.every(unitItem => {
        const isInUnitRange = dataValueDifference < unitItem.difference * 10 ** index;
        if (isInUnitRange) {
          result.difference = unitItem.difference * 10 ** index;
          result.unitPrice = unitItem.unitPrice * 10 ** index;
          result.totalUnits = unitItem.totalUnits;
          breakOut = true;
          return false;
        }
        return true;
      });
      if (breakOut) {
        break;
      }
      index++;
    }

    return result;
  },
  calculateUnit() {
    const minDataValueOfShow = this.canvas.show.minDataValue;
    const { unitPrice, totalUnits  } = this.calculateTransferMap();
    const minFloorValue = Math.floor(minDataValueOfShow / unitPrice) * unitPrice;
    const differenceValue = minDataValueOfShow - minFloorValue;
    this.canvas.xUnit = Math.floor(this.canvas.coordinate.w * 10 / (this.dataAmount + 2)) / 10;
    this.canvas.yUnit = Number( (this.canvas.coordinate.h / totalUnits).toFixed(6) );
    this.canvas.show.minFloorValue = minFloorValue;
    this.canvas.unitPrice = unitPrice;
    this.canvas.yAddHeight = differenceValue / unitPrice * this.canvas.yUnit;
  },
  calculateCoordinateSystemBound() {
    const coordinate = this.canvas.coordinate;
    coordinate.w = this.canvas.width - 2 * this.canvas.xMargin;
    coordinate.h = this.canvas.height - 2 * this.canvas.yMargin;
    coordinate.leftTop = {
      x: this.canvas.xMargin,
      y: this.canvas.yMargin,
    };
    coordinate.leftBottom = {
      x: this.canvas.xMargin,
      y: this.canvas.height - this.canvas.yMargin,
    };
    coordinate.rightTop = {
      x: this.canvas.width - this.canvas.xMargin,
      y: this.canvas.yMargin,
    };
    coordinate.rightBottom = {
      x: this.canvas.width - this.canvas.xMargin,
      y: this.canvas.height - this.canvas.yMargin,
    };
  },
  calculateDataIndexOfFirstShowingPoint() {
    const dataIndexOfFirstShowingPoint = this.dataKeys.length - this.dataAmount + 1;
    const indexInBound = dataIndexOfFirstShowingPoint >= 0;
    return  (indexInBound) ? dataIndexOfFirstShowingPoint : 0;
  },
  getCurrentPoint(sketch) {
    if (this.inDom(sketch)) {
      const mouseX = sketch.mouseX - this.canvas.xMargin - this.canvas.xUnit * 0.5;
      const indexOfPoints = Math.floor(mouseX / this.canvas.xUnit);
      return this.canvas.show.points[indexOfPoints];
    }
  },
  // todo: consider to remove dragged for load more data and change into slider
  loadMoreData(sketch, difference, type = 'dragged', syncLoad = false) {
    const inCanvas = this.inXBound(sketch.mouseX) && this.inYBound(sketch.mouseY);
    const isMasterType = (this.inDom(sketch) && inCanvas);
    const isSyncType = (!this.inDom(sketch) && syncLoad);
    if (isMasterType || isSyncType) {
      if (isMasterType && this.syncLoadMoreData) {
        this.emitLoadMoreData(difference, type);
      }
      const newDataIndexOfFirstShowingPoint = this.canvas.show.atDataIndex - difference;
      const validMoreOldData = (difference === 1 && newDataIndexOfFirstShowingPoint >= 0);
      const validMoreNewData = (difference === -1 && newDataIndexOfFirstShowingPoint < this.dataKeys.length - 5);
      if (validMoreOldData || validMoreNewData) {
        this.canvas.show.originDataIndex = this.canvas.show.atDataIndex;
        this.canvas.show.atDataIndex = newDataIndexOfFirstShowingPoint;
        this.transDataToPoints();
      }
    }
  },
  drawCoordinateSystem(sketch) {
    const canvas = this.canvas;
    const coordinate = canvas.coordinate;
    const leftTop = coordinate.leftTop;
    const rightTop = coordinate.rightTop;
    const leftBottom = coordinate.leftBottom;
    const rightBottom = coordinate.rightBottom;
    const step = coordinate.step;
    const longMarkStep = coordinate.longMarkStep;
    const longMark = coordinate.longMark;
    const shortMarkStep = coordinate.shortMarkStep;
    const shortMark = coordinate.shortMark;
    const leftTextBound = coordinate.leftTextBound;
    const rightTextBound = coordinate.rightTextBound;
    const minFloorValue = canvas.show.minFloorValue;

    // X axis
    const xAxisColor = this.canvas.color.xAxis;
    sketch.stroke(xAxisColor);
    sketch.line(leftBottom.x, leftBottom.y, rightBottom.x, rightBottom.y);
    // x scale & label
    this.canvas.show.points.forEach((point, index) => {
      const long = (index % 5 === 0) ? 10 : 5;
      const dataDate = (index % 5 === 0) ? this.data[point.key]['date'] : undefined;
      // x scale
      sketch.stroke(xAxisColor);
      sketch.line(point.x, rightBottom.y, point.x, rightBottom.y + long);
      // x label
      if (dataDate) {
        const dateArray = dataDate.split(/\//);
        const dateText = `${dateArray[0]}\n${dateArray[1]}/${dateArray[2]}`;
        sketch.fill(xAxisColor);
        sketch.text(dateText, point.x - 14, rightBottom.y + long + 10);
      }
    });

    // Y
    const { unitPrice, totalUnits  } = this.calculateTransferMap();
    const yAxisColor = this.canvas.color.yAxis;
    const yAxisGridColor = this.canvas.color.yAxisGrid;
    sketch.stroke(yAxisColor);
    sketch.line(leftBottom.x, leftBottom.y, leftTop.x, leftTop.y);
    sketch.line(rightBottom.x, rightBottom.y, rightTop.x, rightTop.y);
    // y scale & label
    let singleHeight = 0;
    for (let i = 0; i < totalUnits - 1; i++) {
      const toMarkLongLine = (longMark) ? i % longMarkStep === 0 : undefined;
      const toMarkShortLine = (shortMark) ? i % shortMarkStep === 0 : undefined;

      if (toMarkLongLine || toMarkShortLine) {
        const long = (toMarkLongLine) ? longMark : shortMark;
        const y = leftBottom.y - i * canvas.yUnit;
        sketch.line(leftBottom.x, y, leftBottom.x - long, y);
        sketch.line(rightBottom.x, y, rightBottom.x + long, y);

        if (toMarkLongLine) {
          let value = minFloorValue + i * unitPrice;
          let fixedAmount = (value.toString().length > 15) ? 2 : 0;
          value = value.toFixed(fixedAmount);

          sketch.stroke(yAxisGridColor);
          sketch.line(leftBottom.x, y, rightBottom.x, y);

          sketch.fill(yAxisColor);
          sketch.text(value, leftBottom.x - leftTextBound, y + singleHeight * step);
          sketch.fill(yAxisColor);
          sketch.text(value, rightBottom.x + rightTextBound, y + singleHeight * step);
        }
      }
    }
  },
  drawCrossHair(sketch) {
    // inDom means this is the master control canvas
    let point = undefined;
    if (this.inDom(sketch)) {
      point = (this.freeMove)
        ? { x: sketch.mouseX, y: { mouse: sketch.mouseY}}
        : this.getCurrentPoint(sketch);
    } else if (this.syncCrossHair && this.syncPoint) {
      this.canvas.show.points.every((showPoint) => {
        if (showPoint.key === this.syncPoint.key) {
          point = showPoint;
          return false;
        }
        return true;
      });
    }

    // draw cross hair
    // check point is exist && point is in x axis bound
    let pointIsInBound = (point && this.inXBound(point.x));
    // check all y axis values are in y axis bound
    if (pointIsInBound) {
      Object.keys(point.y).forEach((fieldKey) => {
        const fieldToShow = this.fieldToShow.includes(fieldKey);
        if (fieldToShow) {
          if (!this.inYBound(point.y[fieldKey])) {
            pointIsInBound = false;
            return false;
          }
        }
        return true;
      });
    }

    if (pointIsInBound) {
      const leftBottom = this.canvas.coordinate.leftBottom;
      const rightBottom = this.canvas.coordinate.rightBottom;
      const crossHairColor = this.canvas.color.crossHair;
      const textBoxWidth = this.canvas.crossHair.width;
      const textBoxHeight = this.canvas.crossHair.height;
      const fractionDigits = this.canvas.crossHair.fractionDigits;

      // CrossHair
      // 【line】 vertical part
      sketch.stroke(crossHairColor.line);
      sketch.line(point.x, 0, point.x, this.canvas.height);
      // 【line】 horizontal part
      if (this.showCrossHairY) {
        sketch.stroke(crossHairColor.line);
        Object.keys(point.y).forEach((fieldKey) => {
          const fieldToShow = this.fieldToShow.includes(fieldKey) || fieldKey === 'mouse';
          if (fieldToShow) {
            sketch.line(leftBottom.x, point.y[fieldKey], rightBottom.x, point.y[fieldKey]);
          }
        });
      }
      // 【point】
      Object.keys(point.y).forEach((fieldKey) => {
        const fieldToShow = this.fieldToShow.includes(fieldKey) || fieldKey === 'mouse';
        const dataField = this.dataFields[this.dataFieldsMap[fieldKey]];
        if (fieldToShow) {
          sketch.fill((dataField) ? dataField.color : crossHairColor.point);
          sketch.noStroke();
          sketch.circle(point.x, point.y[fieldKey], this.canvas.pointDiameter);
        }
      });
      // 【text】
      if (this.showCrossHairText) {
        const closeToRight = Math.abs(point.x - leftBottom.x) > Math.abs(point.x - rightBottom.x);
        const fieldKeys = closeToRight ? Object.keys(point.y).reverse() : Object.keys(point.y);
        let showIndex = 0;
        fieldKeys.forEach(fieldKey => {
          const fieldToShow = this.fieldToShow.includes(fieldKey) || fieldKey === 'mouse';
          if (fieldToShow) {
            const { label, color } =  this.dataFields[this.dataFieldsMap[fieldKey]];
            const price = this.data[point.key][fieldKey].toFixed(fractionDigits);
            const text = (label) ? `■ ${label}: ${price}` : `■ : ${price}`;
            sketch.fill(color);
            sketch.noStroke();

            if (closeToRight) {
              sketch.text(text, rightBottom.x - 60 - showIndex * 100, 25);
            } else {
              sketch.text(text, leftBottom.x + showIndex * 100, 25);
            }

            showIndex++;
          }
        });
      }
    }

    // emit the current point to the parent component for sync
    // oly if the free move is false
    if (this.syncCrossHair && !this.freeMove) {
      const key = (point) ? point.key : undefined;
      const index = (point) ? this.canvas.show.points.indexOf(point) : -1;
      this.emitPoint(key, index);
    }
  },
  drawDataPointCurve(sketch) {
    this.drawCurve.forEach((dataField) => {
      const fieldIndex = this.dataFieldsMap[dataField];
      const dataFieldElement = this.dataFields[fieldIndex];
      const color = dataFieldElement.color;
      if (color) {
        sketch.noFill();
        sketch.stroke(color);
        sketch.smooth();
        sketch.beginShape();
        this.canvas.show.points.forEach((point) => {
          const kdsPoint = {x: point.x, y: point.y[dataField]};
          if (this.inCoordinateSystem(kdsPoint)) {
            sketch.vertex(kdsPoint.x, kdsPoint.y);
          }
        });
        sketch.endShape();
      }
    });
  },
  drawBackground(sketch) {
    const color = this.isNightTheme ? 'rgb(28, 36, 52)' : '#fff';
    const sketchColor = sketch.color(color);
    if (this.isNightTheme) {
      sketchColor.setAlpha(0.7 * 255);
    }
    this.canvas.backgroundColor = sketchColor;
    sketch.clear();
    sketch.background(this.canvas.backgroundColor);
  }
};

const baseConfig = {
  props: {
    id: { type: String, required: true },
    data: { type: Object, required: true },
    width: { type: String, default: '100%' },
    height: { default: undefined },
    dataCount: { type: Number, default: 20 },
    freeMove: { type: Boolean, default: false },
    showCrossHairText: { type: Boolean, default: true },
    showCrossHairY: { type: Boolean, default: true },
    syncCrossHair: { type: Boolean, default: true },
    syncPoint: { default: undefined },
    syncLoadMoreData: { type: Boolean, default: true },
    syncMoreDataArgs: { default: undefined },
    drawCurve: { type: Array, default: () => { return []; }},
  },
  data() {
    return {
      canvas: {
        coordinate: {
          step: 1,
          longMarkStep: 5,
          longMark: 10,
          shortMarkStep: 1,
          shortMark: 5,
          leftTextBound: 35,
          rightTextBound: 20,
        },
        width: undefined,
        height: undefined,
        xUnit: 1,
        yUnit: 1,
        xMargin: 70,
        yMargin: 40,
        backgroundColor: '#fff',
        pointDiameter: 10,
        show: {
          atDataIndex: 0,
          maxDataValue: 0,
          minDataValue: 10000000000,
          points: [],
        },
        currentPoint: {},
        dragged: {last: {}, current: {}},
        crossHair: { width: 50, height: 30, fractionDigits: 2 },
        color: {
          xAxis: 'rgba(86,86,86,0.8)',
          yAxis: 'rgba(86,86,86,0.8)',
          yAxisGrid: 'rgba(86,86,86,0.2)',
          crossHair: {
            line: '#3f3f3f4f',
            point: '#ff8888',
            textBoxBG: 'rgba(43,65,107,0.9)',
            textBoxBorder: '#36b4ed',
            text: '#ffe3b4'
          },
        },
      },
      dataFields: [], // { dataField, label, color }
      fieldToShow: [...this.drawCurve],
      lockP5Drawing: false,
    };
  },
  computed: {
    dataKeys() {
      return Object.keys(this.data);
    },
    dataAmount() {
      if (5 > this.dataCount) {
        return 5;
      } else if (this.dataKeys.length < this.dataCount) {
        return this.dataKeys.length;
      }
      return this.dataCount;
    },
    dataFieldsMap() {
      const result = {};
      this.dataFields.forEach(({dataField}, index) => {
        result[dataField] = index;
      });
      return result;
    },
    ...mapGetters(['theme']),
    isNightTheme() {
      return this.theme === 'night-theme';
    },
  },
  watch: {
    dataAmount() {
      if (this.canvas.coordinate.leftBottom) {
        this.transDataToPoints();
      }
    },
    syncPoint() {
      const canvas = this.canvas;
      if (!this.inDom(this.sketch)) {
        if (this.syncCrossHair && this.syncPoint) {
          let hasMathPoint = false;
          canvas.show.points.every((showPoint) => {
            if (this.syncPoint.key === showPoint.key) {
              canvas.currentPoint = this.data[this.syncPoint.key];
              hasMathPoint = true;
              return false;
            }
            return true;
          });
          if (!hasMathPoint) {
            canvas.currentPoint = {};
          }
        } else {
          canvas.currentPoint = {};
        }
      }
    },
    syncMoreDataArgs() {
      if (!this.inDom(this.sketch) && this.syncLoadMoreData && this.syncMoreDataArgs) {
        this.loadMoreData(this.sketch, this.syncMoreDataArgs.difference, this.syncMoreDataArgs.type, true);
      }
    },
  },
  methods: {
    setColor(theme) {
      const lightThemeColor = {
        xAxis: 'rgba(86,86,86,0.8)',
        yAxis: 'rgba(86,86,86,0.8)',
        yAxisGrid: 'rgba(86,86,86,0.2)',
        crossHair: {
          line: '#3f3f3f4f',
          point: '#ff8888',
          textBoxBG: 'rgba(43,65,107,0.9)',
          textBoxBorder: '#36b4ed',
          text: '#ffe3b4'
        },
      };

      const nightThemeColor = {
        xAxis: 'rgb(136,136,136)',
        yAxis: 'rgb(136,136,136)',
        yAxisGrid: 'rgba(213,213,213, 0.6)',
        crossHair: {
          line: 'rgb(201,177,140)',
          point: '#ff8888',
          textBoxBG: 'rgba(43,65,107,0.9)',
          textBoxBorder: '#36b4ed',
          text: '#ffe3b4'
        },
      };
      if (theme) {
        this.canvas.color = (theme === 'night-theme') ? nightThemeColor : lightThemeColor;
      }
    }
  },
  mounted() {
    const showDataAt = this.calculateDataIndexOfFirstShowingPoint();
    this.canvas.show.atDataIndex = (showDataAt >= 0) ? showDataAt : 0;
    this.setColor(this.theme);
  },
};

export {
  eventsNames,
  domEventNames,
  script,
  actions,
  emits,
  events,
  functions,
  baseConfig,
};
