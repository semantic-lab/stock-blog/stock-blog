import { validators as api } from '../configs/request-validator/user';

export default async ({ app, store }) => {
  const hasToken = store.getters.hasToken;
  const hasUser = store.getters['user/hasUser'];
  const isLoginButNeedsToAccessUserInfo = hasToken && !hasUser;

  if (isLoginButNeedsToAccessUserInfo) {
    try {
      const { data } = await app.$api(api.AccessUserRequest, { userId: null });
      await store.dispatch('user/setUser', data);
    } catch (error) {
      app.$apiErrorHandler(error, () => {
        store.dispatch('removeToken');
      });
    }
  }
};
