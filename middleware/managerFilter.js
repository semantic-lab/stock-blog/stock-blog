export default ({ store, redirect }) => {
  const hasToken = store.getters.hasToken;
  const isManager = store.getters['user/isManager'];

  if (!(hasToken && isManager)) {
    redirect('/');
  }
};
