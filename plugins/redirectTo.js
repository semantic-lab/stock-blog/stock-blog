export default ({ store }, inject) => {
  inject('redirectTo', async function (name, params = undefined, query = undefined) {
    if (process.client) {
      await store.dispatch('view/setDisplayBodyBlock', true);
      this.$router.push({ name, params, query });
    }
  });
};
