export default function (context, inject) {
  inject('conditionMutator', (request, { column, value }) => {
    if (!request.hasOwnProperty('conditions')) {
      request['conditions'] = [];
    }

    const hasCondition = column && value !== undefined && value !== null;
    if (hasCondition) {
      request['conditions'].push({ column, value });
    }
  });
};
