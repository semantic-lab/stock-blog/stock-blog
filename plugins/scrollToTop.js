export default (context, inject) => {
  inject('scrollToTop', () => {
    if (process.client) {
      document.getElementsByClassName('body')[0].scrollTo(0, 0);
    }
  });
};
