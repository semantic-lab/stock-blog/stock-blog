export default ({ store }, injection) => {
  injection('displayBodyBlock', (display = true) => {
    if (process.client) {
      store.dispatch('view/setDisplayBodyBlock', display);
    }
  });
};
