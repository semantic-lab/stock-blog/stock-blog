import APIError from "../helpers/APIError";

export default ({ redirect }, inject) => {
  inject('apiErrorHandler', function (error, callback = undefined) {
    if (error instanceof APIError) {
      const hasCustomAction = !!callback;
      if (hasCustomAction) {
        const errorInfo = error.toObject();
        callback(errorInfo);
      } else {
        switch (error.status) {
          case APIError.CLIENT_REQUEST_VALIDATION.status:
            return error.data.error;
          case APIError.SERVER_REQUEST_VALIDATION.status:
            return error.data.error;
          case APIError.SERVER.status:
            console.error(error.status, error.message);
            console.error(error.data);
            // redirect('/error/api-server');
            break;
          case APIError.INVOKE.status:
            if (process.client) {
              this.$Notice.error({
                title: `${error.data.message} (${error.data.code})`
              });
            }
            break;
        }
      }
    } else {
      throw error;
    }
  });
};
