import axios from 'axios';
import { apiRoot, status } from '../configs/api';
import APIError from '../helpers/APIError';
import { LaraValidator } from 'lara-validator';

axios.defaults.baseURL = apiRoot;

const _requestValidation = (rule, requestData) => {
  const requiredArgumentsAreSet = rule && requestData;
  if (requiredArgumentsAreSet) {
    const validator = new LaraValidator(rule, requestData);
    const isValid = validator.valid();
    if (!isValid) {
      throw new APIError(
        APIError.CLIENT_REQUEST_VALIDATION,
        {
          error: validator.errorMessage,
        }
      );
    }
  }
};

const _apiErrorGenerator = axiosErrorResponse => {
  const httpStatus = axiosErrorResponse.status;
  const isRequestValidationError = status.requestValidationError === httpStatus;
  const isAPIError = status.apiError === httpStatus;
  const isServerError = !isRequestValidationError && !isAPIError;

  if (isRequestValidationError) {
    throw new APIError(
      APIError.SERVER_REQUEST_VALIDATION,
      {
        error: axiosErrorResponse.data.data,
      }
    );
  }

  if (isAPIError) {
    throw new APIError(
      APIError.INVOKE,
      axiosErrorResponse.data,
    )
  }

  if (isServerError) {
    throw new APIError(
      APIError.SERVER,
      {
        httpStatus,
        error: axiosErrorResponse.data,
      }
    );
  }
};

const _JSONToFormData = (jsonData) => {
  const formData = new FormData();
  _parsingJSON(formData, jsonData);
  return formData;
};

const _parsingJSON = (formData, data, parentKey) => {
  const isCurlyBracketsObject = data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File);
  if (isCurlyBracketsObject) {
    Object.keys(data).forEach(key => {
      const nextParentKey = (parentKey) ? `${parentKey}[${key}]` : key;
      _parsingJSON(formData, data[key], nextParentKey);
    });
  } else {
    const value = data || '';
    formData.append(parentKey, value);
  }
};

const _invokeAPI = async (method, url, requestData, token) => {
  const headers = {
    Authorization: `Bearer ${token}`,
  };
  try {
    let axiosResponse = undefined;
    switch (method) {
      case 'get':
        axiosResponse = await axios.get(url, { headers });
        break;
      case 'post':
        axiosResponse = await axios.post(url, requestData, { headers });
        break;
      case 'form':
        requestData = _JSONToFormData(requestData);
        axiosResponse = await axios.post(url, requestData, { headers });
        break;
    }

    const invokeAPIFailed = status.apiError === axiosResponse.status;
    if (invokeAPIFailed) {
      _apiErrorGenerator(axiosResponse);
    }

    return axiosResponse.data;

  } catch (error) {
    const isAPIError = !!error.response;
    if (isAPIError) {
      _apiErrorGenerator(error.response);
    }
    throw error;
  }
};

export default ({ store }, inject) => {
  inject('api', async (apiRequestConfig, requestData = undefined, urlKey = undefined, closingBodyBlockAfterResponse = true) => {
    if (process.client) {
      await store.dispatch('view/setDisplayBodyBlock', true);
    }
    const method = apiRequestConfig.method;
    const url = (urlKey) ? apiRequestConfig.url[urlKey] : apiRequestConfig.url;
    const requestRules = apiRequestConfig.body;
    const token = store.state.token;
    _requestValidation(requestRules, requestData);
    const response = await _invokeAPI(method, url, requestData, token);
    if (process.client && closingBodyBlockAfterResponse) {
      await store.dispatch('view/setDisplayBodyBlock', false);
    }
    return response;
  });
}
