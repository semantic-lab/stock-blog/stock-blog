# Path mapping

## Manager
action              | path                                        | comment
--------------------|:--------------------------------------------|:----------
**note**            |                                             |
note list           | /note/index.vue                             |
note detail         | /note/_id/index.vue                         |
**personal**        |                                             |
personal detail     | /personal/index.vue                         | simple form + password update
**stock data**      |                                             |
action cards        | /stock-data/index.vue                       | a sub-route for view all sub-actions
update data         | /stock-data/update.vue                      | upload file & invoke data transfer to DB
stock tag           | /stock-data/tag.vue                         | CRUD of stock tag
**user**            |                                             |
user list           | /user/index.vue                             |
user detail         | /user/_id/index.vue                         | simple form + password update + identify type update

## Investor
action              | path                                        | comment
--------------------|:--------------------------------------------|:----------
**news**            |                                             |
news list           | /news/index.vue                             |
**note**            |                                             |
note list           | /note/index.vue                             |
note detail         | /note/_id/index.vue                         |
**personal**        |                                             |
personal detail     | /personal/index.vue                         | simple form + password update
**stock**           |                                             |
action cards        | /stock/index.vue                            | a sub-route for view all sub-actions
single stock info   | /stock/_id/index.vue                        | include stock data, news, note and tags
multi stocks        | /stock/compare.vue                          | only basic stock data for compare

