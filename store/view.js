const MODULE = 'VIEW';
const SET_DISPLAY_USER_INFO_BOX = `${MODULE}/SET_DISPLAY_USER_INFO_BOX`;
const SET_DISPLAY_BODY_BLOCK = `${MODULE}/SET_DISPLAY_BODY_BLOCK`;

// state
const state = () => {
  return {
    displayUserInfoBox: false,
    displayBodyBlock: false,
  };
};

// mutations
const mutations = {
  [SET_DISPLAY_USER_INFO_BOX](state, display) {
    const displayIsSet = display !== undefined && display !== null;
    if (displayIsSet) {
      state.displayUserInfoBox = display;
    } else {
      state.displayUserInfoBox = !state.displayUserInfoBox;
    }
  },
  [SET_DISPLAY_BODY_BLOCK](state, display) {
    const displayIsSet = display !== undefined && display !== null;
    if (displayIsSet) {
      state.displayBodyBlock = display;
    } else {
      state.displayBodyBlock = !state.displayBodyBlock;
    }
  }
};

// actions
const actions = {
  setDisplayUserInfoBox({commit, state}, display) {
    commit(SET_DISPLAY_USER_INFO_BOX, display);
  },
  setDisplayBodyBlock({commit, state}, display) {
    commit(SET_DISPLAY_BODY_BLOCK, display);
  },
};

// getters
const getters = {
  displayUserInfoBox(state) {
    return state.displayUserInfoBox;
  },
  displayBodyBlock(state) {
    return state.displayBodyBlock;
  }
};

export default {
  state,
  mutations,
  actions,
  getters,
  namespaced: true,
};

