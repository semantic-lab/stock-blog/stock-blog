const MODULE = 'COMPANY';
const SET_COMPANIES = `${MODULE}/SET_COMPANIES`;
const CLEAR_COMPANIES = `${MODULE}/CLEAR_COMPANIES`;

const state = () => {
  return {
    companies: undefined,
  };
};

const mutations = {
  [SET_COMPANIES](state, companies) {
    state.companies = companies;
  },
  [CLEAR_COMPANIES](state) {
    state.companies = undefined;
  },
};

const actions = {
  setCompanies({ commit }, companies) {
    commit(SET_COMPANIES, companies);
  },
  clearCompanies({ commit }) {
    commit(CLEAR_COMPANIES)
  },
};

const getters = {
  hasCompanies(state) {
    try {
      if (Array.isArray(state.companies)) {
        return state.companies.length > 0;
      } else {
        return Object.keys(state.companies).length > 0;
      }
    } catch (error) {
      return false;
    }
  },
  companies(state) {
    return state.companies;
  }
};

export default {
  state,
  mutations,
  actions,
  getters,
  namespace: true,
};
