import { apiRoot } from '../configs/api';
const MODULE = 'USER';
const SET_USER = `${MODULE}/SET_USER`;
const SET_USER_PROP = `${MODULE}/SET_USER_PROP`;

const defaultImage = 'https://i.ytimg.com/vi/s7_2vC7BoCs/maxresdefault.jpg';

// state
const state = () => {
  return {
    user: {
      id: undefined,
      name: undefined,
      email: undefined,
      phone: undefined,
      image: defaultImage,
      type: undefined,
    },
  };
};

const mutations = {
  [SET_USER](state, user) {
    state.user = {
      ...state.user,
      ...user,
    };
    const hasUserImage = state.user.image !== defaultImage;
    if (hasUserImage) {
      state.user.image = `${apiRoot}/v1${user.image}`;
    }
  },
  [SET_USER_PROP](state, { key, value }) {
    const stateUserHasKey = state.user.hasOwnProperty(key);
    if (stateUserHasKey) {
      const isUserImage = key === 'image';
      state.user[key] = isUserImage ? `${apiRoot}/v1${value}` : value;
    }
  },
};

const actions = {
  setUser({commit, state}, user) {
    if (user) {
      commit(SET_USER, user);
    }
  },
  setUserProp({commit, state}, {key, value}) {
    if (key && value) {
      commit(SET_USER_PROP, {key, value});
    }
  },
  removeUser({ commit, state }) {
    const user = {
      id: undefined,
      name: undefined,
      email: undefined,
      phone: undefined,
      image: defaultImage,
      type: undefined,
    };
    commit(SET_USER, user);
  }
};

const getters = {
  hasUser(state) {
    return !!state.user.id;
  },
  isManager(state) {
    return state.user.type === 'dev' || state.user.type === 'manager';
  },
  isUser(state) {
    return state.user.type === 'user';
  },
  user(state) {
    return state.user;
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
  namespaced: true,
};
