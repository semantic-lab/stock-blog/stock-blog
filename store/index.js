import Cookies from 'js-cookie';
const cookieparser = process.server ? require('cookieparser') : undefined;

const MODULE = 'SERVER_STORAGE';
const SET_TOKEN = `${MODULE}/SET_TOKEN`;
const SET_THEME = `${MODULE}/SET_THEME`;
const REMOVE_TOKEN = `${MODULE}/REMOVE_TOKEN`;

const getCookie = (cookieObject, key) => {
  try {
    return cookieObject[key];
  } catch (error) {
    return undefined;
  }
};

const state = () => {
  return {
    token: undefined,
    theme: 'night-theme',
  };
};

const mutations = {
  [SET_TOKEN](state, authToken) {
    Cookies.set('token', authToken);
    state.token = authToken;
  },
  [SET_THEME](state, themeName) {
    Cookies.set('theme', themeName);
    state.theme = themeName || 'night-theme';
  },
  [REMOVE_TOKEN](state) {
    Cookies.remove('token');
    state.token = undefined;
  },
};

const actions = {
  // server side access data and set back to state
  nuxtServerInit({ commit }, { req }) {
    let token = undefined;
    let theme = undefined;
    const hasCookieInReq = !!req.headers.cookie;
    if (hasCookieInReq) {
      try {
        const allCookies = cookieparser.parse(req.headers.cookie);
        token = getCookie(allCookies, 'token');
        theme = getCookie(allCookies, 'theme');
      } catch (error) {
        // ignore
      }
    }
    commit(SET_TOKEN, token);
    commit(SET_THEME, theme);
  },
  // client side access data from vue instance
  setToken({commit, state}, token) {
    commit(SET_TOKEN, token);
  },
  setTheme({commit, state}, themeName) {
    commit(SET_THEME, themeName);
  },
  removeToken({ commit, state }) {
    commit(REMOVE_TOKEN);
  },
};

const getters = {
  hasToken(state) {
    return !!state.token;
  },
  token(state) {
    return state.token;
  },
  theme(state) {
    return state.theme;
  }
};

export default {
  state,
  mutations,
  actions,
  getters,
  namespaced: true,
};
